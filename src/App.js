import { useEffect, useState, useCallback } from "react";
import CountrySelector from "./component/countrySelector";
import Highlight from "./component/highlight";
import Summary from "./component/summary";
import { getCountries, getReportByCountry } from "./apis";



function App() {
  const [countries, setCountries] = useState([]);
  const [selectedCountryId, setSelectedCountryId] = useState('');
  const [report, setReport] = useState([]);

  useEffect(() => {
    getCountries()
      .then(res => {
        setCountries(res.data);
        setSelectedCountryId('vn')
      })
  }, [])

  const handleOnChange = useCallback((e) => {
    setSelectedCountryId(e.target.value);
  }, []);

  useEffect(() => {
    if (selectedCountryId) {
      const selectedCountry = countries.find(
        (country) => country.ISO2 === selectedCountryId.toUpperCase()
      );
      getReportByCountry(selectedCountry.Slug).then((res) => {
        console.log('getReportByCountry', { res });
        // remove last item = current date
        res.data.pop();
        setReport(res.data);
      });
    }
  }, [selectedCountryId, countries]);


  return (
    <div className="App">
      <CountrySelector countries={countries} handleOnChanges={handleOnChange} value={setSelectedCountryId}></CountrySelector>
      <Highlight report={report}></Highlight>
      <Summary report={report}></Summary>
    </div>
  );
}

export default App;
