import { Grid } from '@material-ui/core'
import React from 'react'
import LineChart from '../chart/lineChart'

const Summary = ({ report }) => {
    return (
        <Grid container spacing={3}>
            <Grid item sm={8} xs={12}>
                <LineChart data={report}></LineChart>
            </Grid>
            <Grid item sm={4} xs={12}></Grid>
        </Grid>
    )
}

export default Summary