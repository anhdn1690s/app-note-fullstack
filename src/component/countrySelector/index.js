import { FormControl, FormHelperText, InputLabel, NativeSelect } from '@material-ui/core'
import React from 'react'

const CountrySelector = ({ value, handleOnChanges, countries }) => {
    return (
        <FormControl >
            <InputLabel shrink htmlFor='country-selector'>
                Quốc Gia
            </InputLabel>
            <NativeSelect
                value={value}
                onChange={handleOnChanges}
                inputProps={{
                    name: 'country',
                    id: 'country-selector',
                }}
            >
                {countries.map((Country) => (
                    <option key={Country.ISO2} value={Country.ISO2.toLowerCase()}>
                        {Country.Country}
                    </option>
                ))}
            </NativeSelect>
            <FormHelperText>Lựa chọn quốc gia</FormHelperText>
        </FormControl>
    )
}
export default CountrySelector