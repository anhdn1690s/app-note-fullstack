import { Grid } from '@material-ui/core'
import { type } from '@testing-library/user-event/dist/type';
import HighlightCard from './highlightCard';

const Highlight = ({ report }) => {
    const data = report && report.length ? report[report.length - 1] : [];

    const summary = [
        {
            title: "so ca nhiem",
            count: data.Confirmed,
            type: "confirmed",
        },
        {
            title: "so ca khoi",
            count: data.Active,
            type: "active",
        },
        {
            title: "so ca tu vong",
            count: data.Deaths,
            type: "deaths",
        }
    ]
    return (
        <Grid container spacing={3}>
            {
                summary.map((item, key) =>
                    <Grid item sm={4} xs={12} key={key}>
                        <HighlightCard title={item.title} count={item.count} type={item.type}></HighlightCard>
                    </Grid>
                )
            }
        </Grid>
    )
}

export default Highlight


